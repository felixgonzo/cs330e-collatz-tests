#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
# a test that is suppossed to confirm the outputs of read are ints
    def test_read_2(self):
        s = "321 469\n"
        i, j = collatz_read(s)
        self.assertEqual(isinstance(i,int), True)
        self.assertEqual(isinstance(j,int), True)
# this confirms the length of the list output from read is of length 2
    def test_read_3(self):
        s = "1234 8954\n"
        x = collatz_read(s)
        self.assertEqual(len(x),2)
# checks that it does split it
    def test_read_4(self):
        s = "21432 232\n"
        self.assertEqual(s.split(), ['21432','232'])
        with self.assertRaises(TypeError):
            s.split(2)

    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 469)
        self.assertEqual(v, 144)

    def test_eval_2(self):
        v = collatz_eval(1, 6533)
        self.assertEqual(v, 262)

    def test_eval_3(self):
        v = collatz_eval(1, 8539)
        self.assertEqual(v, 262)

    def test_eval_4(self):
        v = collatz_eval(9999, 1233)
        self.assertEqual(v, 262)
# confirms the output of eval is an int
    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(isinstance(v,int),True)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 50,73,5043)
        self.assertEqual(w.getvalue(), "50 73 5043\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100,174,202)
        self.assertEqual(w.getvalue(), "100 174 202\n")

# confirms the output is a string
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1,33,23)
        self.assertEqual(isinstance(w.getvalue(), str),True)

#confirms that the values in the string are infact digits
    def test_print_4(self):
        w = StringIO()
        collatz_print(w,100,1231,3241)
        s = w.getvalue()
        s = s.replace(" ", "")
        for i in s.split():
            self.assertEqual(i.isdigit(),True)

    # -----
    # solve
    # -----
# confirms that proccess start to finish inputing proper values spitting out right
# number of steps.


    def test_solve_1(self):
        r = StringIO("1 999\n1 2345\n1 7878\n1 6565\n")
        w = StringIO()
        collatz_solve(r,w)
        self.assertEqual(w.getvalue(), "1 999 179\n1 2345 183\n1 7878 262\n1 6565 262\n")

    def test_solve_2(self):
        r = StringIO("4 13\n23 200\n19 210\n94 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "4 13 20\n23 200 125\n19 210 125\n94 1000 179\n")

# confirms the output is infact a string
    def test_solve_3(self):
        r = StringIO("23 19\n14 93\n666 444\n9993 2341\n")
        w = StringIO()
        collatz_solve(r,w)
        self.assertEqual(isinstance(w.getvalue(),str),True)

# confirms that the characters are infact digits.
    def test_solve_4(self):
        r = StringIO("4 13\n23 200\n19 210\n94 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        s = w.getvalue()
        s = s.replace(" ", "")
        for i in s.split():
            self.assertEqual(i.isdigit(),True)


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
