#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "5 15\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  5)
        self.assertEqual(j, 15)

    def test_read3(self):
        s = "30 40\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  30)
        self.assertEqual(j, 40)

    def test_read4(self):
        s = "99 105\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  99)
        self.assertEqual(j, 105)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 19)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 124)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 88)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 173)

    def test_eval_5(self):
        v = collatz_eval(5, 15)
        self.assertEqual(v, 19)

    def test_eval_6(self):
        v = collatz_eval(30, 40)
        self.assertEqual(v, 106)

    def test_eval_7(self):
        v = collatz_eval(99, 105)
        self.assertEqual(v, 87)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 2, 11, 21)
        self.assertEqual(w.getvalue(), "2 11 21\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 5, 15, 25)
        self.assertEqual(w.getvalue(), "5 15 25\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 8, 18, 28)
        self.assertEqual(w.getvalue(), "8 18 28\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 19\n100 200 124\n201 210 88\n900 1000 173\n")

    def test_solve2(self):
        r = StringIO("2 12\n22 33\n202 211\n905 1001\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 12 19\n22 33 111\n202 211 88\n905 1001 173\n")

    def test_solve3(self):
        r = StringIO("2 3\n4 5\n50 60\n700 800\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 3 7\n4 5 5\n50 60 112\n700 800 170\n")

    def test_solve4(self):
        r = StringIO("15 16\n110 130\n200 210\n1 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "15 16 17\n110 130 121\n200 210 88\n1 100 118\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
